//
//    @file pool.c
//    @author Etienne Malaboeuf
//    @brief
//    @version 0.1
//    @date 2020-03-04
//
//    Copyright 2020 Etienne Malaboeuf

//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at

//        http://www.apache.org/licenses/LICENSE-2.0

//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//

#include <SPLB/bench.h>
#include <SPLB/pool.h>
#include <SPLB/rand/xorshift128p.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

void test1()
{
    splb_ObjectPool_t pool;
    if(splb_pool_Init(&pool, 10, sizeof(int), NULL) < 0)
    {
        perror("Init");
        return;
    }

    int* array_of_tree = splb_pool_Get(&pool);
    printf("3 '%p'\n", (void*)array_of_tree);

    int* array_of_one = splb_pool_Get(&pool);
    printf("1 %p\n", (void*)array_of_one);

    int* array_of_tree_2 = splb_pool_Get(&pool);
    printf("3_2 %p\n", (void*)array_of_tree_2);

    int* array_of_tree_3 = splb_pool_Get(&pool);
    printf("3_3 %p\n", (void*)array_of_tree_3);

    printf("Free 1\n");
    splb_pool_Release(&pool, array_of_one);

    array_of_one = splb_pool_Get(&pool);
    printf("1 %p\n", (void*)array_of_one);

    printf("Free 3_3\n");
    splb_pool_Release(&pool, array_of_tree_3);
    printf("Free 3\n");
    splb_pool_Release(&pool, array_of_tree);

    array_of_tree = splb_pool_Get(&pool);
    printf("3 %p\n", (void*)array_of_tree);

    array_of_tree_3 = splb_pool_Get(&pool);
    printf("3_3 %p\n", (void*)array_of_tree_3);

    splb_pool_DeInit(&pool);
}

splb_TimeNs_t get_n(splb_ObjectPool_t* pool, size_t nb_to_allocate)
{
    const size_t step  = 100000;
    const size_t nb_op = nb_to_allocate;

    if(nb_op == 0)
    {
        return 0;
    }

    // printf("Getting %ld blocks.\n", nb_op);

    splb_TimeNs_t total = 0;
    splb_TimeNs_t start = splb_bench_GetCurrentTimeNs();
    {
        size_t counter = 0;
        for(size_t i = 0; i < nb_op; ++i)
        {

            // this is a bad benchmark, we need to test it on a fragmented pool.
            char* mem = splb_pool_Get(pool);

            memset(mem, 0, 8 * 8);
            if(++counter % step == 0)
            {
                // printf("Got %ld.\n", step);
                splb_TimeNs_t end = splb_bench_GetCurrentTimeNs();

                // printf("It took: %ld ms or %lfms / get, counter is: %ld, total is: %ldms\n",
                //        (end - start) / (1000 * 1000),
                //        ((end - start) / (1000 * 1000)) / (double)step,
                //        counter,
                //        total / (1000 * 1000));

                // Check before adding to total so we can include the last value after the timer timed out.
                if(total > (5ULL * 1000ULL * 1000ULL * 1000ULL))
                {
                    // printf("Time out.\n");
                    total += (end - start);
                    break;
                }

                total += (end - start);
                start = end;
            }
        }
    }

    // printf("Avg: %lfms / get.\n", (total / (1000 * 1000)) / (double)nb_op);

    return total / nb_op;
}

splb_TimeNs_t bench_frag_random_N_percent_free(size_t free_percent)
{
    const size_t obj_count = 1024 * 1024 * 10;
    const size_t obj_size  = 8 * 8;

    splb_rand_xorshift128p_State_t rand_state;
    splb_rand_xorshift128p_InitState(&rand_state, time(NULL));

    splb_ObjectPool_t pool;
    if(splb_pool_Init(&pool, obj_count, obj_size, NULL) < 0) // should be count the init time too ?
    {
        perror("Init");
        return 0;
    }

    // printf("bench_frag_random_%ld_percent_free.\n", free_percent);
    // printf("Emptying the pool.\n");
    for(size_t i = 0; i < obj_count; ++i)
    {
        *((char*)splb_pool_Get(&pool)) = 1;
    }

    // printf("Releasing sporadically %ld blocks.\n", (obj_count * free_percent) / 100);

    for(size_t i = 0; i <= (obj_count * free_percent) / 100; ++i)
    {
        for(;;)
        { // Dont 'free' a used block.
            const size_t idx = splb_rand_xorshift128p_NextU64(&rand_state) % obj_count;
            if(*((char*)pool.headers_ + idx) == 0)
            {
                splb_pool_Release(&pool, (char*)pool.mem_lo_ + idx * pool.block_size_);
                break;
            }
        }
    }

    splb_TimeNs_t time = get_n(&pool, (obj_count * free_percent) / 100);

    splb_pool_DeInit(&pool);

    return time;
}


int main()
{
    // test1();

    for(int i = 0; i < 100; ++i)
    {
        splb_TimeNs_t time   = bench_frag_random_N_percent_free(i);
        double        metric = (double)time * (double)time / ((100. - (double)i) * (100. - (double)i));

        printf("-> For %d percent randomly freed, %ld ns/op. Metric: %lf\n\n", i, time, metric);
    }

    return 0;
}
