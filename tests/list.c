//
//    @file list.c
//    @author Etienne Malaboeuf
//    @brief
//    @version 0.1
//    @date 2020-03-04
//
//    Copyright 2020 Etienne Malaboeuf

//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at

//        http://www.apache.org/licenses/LICENSE-2.0

//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//

#include <SPLB/list.h>
#include <SPLB/pool.h>
#include <stdio.h>
#include <stdlib.h>

void PrintList(/* const */ splb_List_t* list)
{
    if(list != NULL)
    {
        for(splb_list_Iterator_t it = splb_list_Begin(list), end = splb_list_End(list);
            splb_list_IteratorNotEquals(&it, &end);
            splb_list_IteratorIncrement(&it))
        {
            printf("%d ", *splb_list_IteratorGet(&it));
        }
    }
}

void test_1()
{
#if defined(SPL_LIST_USE_POOL)
    splb_ObjectPool_t pool;
    splb_pool_Init(&pool, 1024, SPL_LIST_POOL_BLOCK_SIZE, NULL);

    splb_List_t list;
    splb_list_Init(&list, &pool);
#else
    splb_List_t list;
    splb_list_Init(&list);
#endif


    printf("Add 5, 3, 1\n");
    splb_list_ValueType_t val = 5;
    splb_list_PushFront(&list, &val);
    val = 3;
    splb_list_PushFront(&list, &val);
    val = 1;
    splb_list_PushFront(&list, &val);

    printf("Size: %zd | Empty: %d\n", splb_list_Size(&list), splb_list_Empty(&list));
    PrintList(&list);
    putc('\n', stdout);
    ////////////////////////////////////////////////////////////////////////////

    printf("Add 6, 7\n");
    val = 6;
    splb_list_PushBack(&list, &val);
    val = 7;
    splb_list_PushBack(&list, &val);

    printf("Size: %zd | Empty: %d\n", splb_list_Size(&list), splb_list_Empty(&list));
    PrintList(&list);
    putc('\n', stdout);
    ////////////////////////////////////////////////////////////////////////////

    printf("Add 2\n");
    val = 2;
    splb_list_Insert(&list, splb_list_Begin(&list), &val);

    printf("Size: %zd | Empty: %d\n", splb_list_Size(&list), splb_list_Empty(&list));
    PrintList(&list);
    putc('\n', stdout);
    ////////////////////////////////////////////////////////////////////////////

    printf("Delete 3\n");
    splb_list_Iterator_t it = splb_list_Begin(&list);
    splb_list_IteratorIncrement(&it);
    splb_list_IteratorIncrement(&it);

    splb_list_EraseIt(&list, it);

    printf("Size: %zd | Empty: %d\n", splb_list_Size(&list), splb_list_Empty(&list));
    PrintList(&list);
    putc('\n', stdout);
    ////////////////////////////////////////////////////////////////////////////

    splb_list_DeInit(&list);
}

void test_2()
{
#if defined(SPL_LIST_USE_POOL)
    splb_ObjectPool_t pool;
    splb_pool_Init(&pool, 1024, SPL_LIST_POOL_BLOCK_SIZE, NULL);

    splb_List_t list;
    splb_list_Init(&list, &pool);
#else
    splb_List_t list;
    splb_list_Init(&list);
#endif

    puts("Initialised");

    printf("Size: %zd | Empty: %d\n", splb_list_Size(&list), splb_list_Empty(&list));
    PrintList(&list);
    puts("");

    splb_list_ValueType_t val = 11;
    splb_list_PushFront(&list, &val);

    printf("Size: %zd | Empty: %d\n", splb_list_Size(&list), splb_list_Empty(&list));
    PrintList(&list);
    puts("");

    splb_list_PopBack(&list);

    printf("Size: %zd | Empty: %d\n", splb_list_Size(&list), splb_list_Empty(&list));
    PrintList(&list);
    puts("");

    splb_list_DeInit(&list);
}

void test_3()
{
#if defined(SPL_LIST_USE_POOL)
    splb_ObjectPool_t pool;
    splb_pool_Init(&pool, 120000000LU, SPL_LIST_POOL_BLOCK_SIZE, NULL);
#endif

    // on this benchmark, we cream malloc, it might not be the same with a fragmented pool though.
    for(size_t i = 1; i <= 5; ++i)
    {
        splb_List_t list;

        // getc(stdin);
        printf("Started\n");
        splb_list_ValueType_t val = 0;
#if defined(SPL_LIST_USE_POOL)
        splb_list_InitNVal(&list, &pool, (i * 120000000LU) / 5, &val); // 256000000LU
#else
        splb_list_InitNVal(&list, (i * 120000000LU) / 5, &val); // 256000000LU
#endif
        printf("Ended\n");
        printf("Size: %zd | Empty: %d\n", splb_list_Size(&list), splb_list_Empty(&list));
        // getc(stdin);
        printf("Started\n");
        splb_list_Clear(&list);
        printf("ended\n");
        printf("Size: %zd | Empty: %d\n", splb_list_Size(&list), splb_list_Empty(&list));
        // getc(stdin);

        splb_list_DeInit(&list);
    }
}

void test_4()
{
#if defined(SPL_LIST_USE_POOL)
    splb_ObjectPool_t pool;
    splb_pool_Init(&pool, 1024, SPL_LIST_POOL_BLOCK_SIZE, NULL);
#endif

    splb_List_t list;
    splb_list_Init(&list, &pool); // 256000000LU

    int val = 1;
    splb_list_PushBack(&list, &val);
    val = 2;
    splb_list_Insert(&list, splb_list_End(&list), &val);
    val = 3;
    splb_list_Insert(&list, splb_list_End(&list), &val);
    val = 4;
    splb_list_Insert(&list, splb_list_End(&list), &val);
    PrintList(&list);
    splb_list_DeInit(&list);
}

int main()
{
    // test_1();
    // test_2();
    test_3();
    // test_4();
    return 0;
}
