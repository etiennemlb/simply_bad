//
//    @file deque.c
//    @author Etienne Malaboeuf
//    @brief
//    @version 0.1
//    @date 2020-03-04
//
//    Copyright 2020 Etienne Malaboeuf

//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at

//        http://www.apache.org/licenses/LICENSE-2.0

//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//

#include <SPLB/deque.h>
#include <stdio.h>
#include <stdlib.h>

void PrintList(const splb_Deque_t* deque)
{
    if(deque != NULL)
    {
        for(splb_deque_Iterator_t it = splb_deque_Begin(deque), end = splb_deque_End(deque);
            splb_deque_IteratorNotEquals(&it, &end);
            splb_deque_IteratorIncrement(&it))
        {
            printf("%d ", *splb_deque_IteratorGet(&it));
        }
    }
}

void test_1()
{
    splb_Deque_t deque;
    splb_deque_Init(&deque);

    printf("Add 5, 3, 1\n");
    splb_deque_ValueType_t val = 5;
    splb_deque_PushFront(&deque, &val);
    val = 3;
    splb_deque_PushFront(&deque, &val);
    val = 1;
    splb_deque_PushFront(&deque, &val);

    printf("Size: %zd | Empty: %d\n", splb_deque_Size(&deque), splb_deque_Empty(&deque));
    PrintList(&deque);
    putc('\n', stdout);
    ////////////////////////////////////////////////////////////////////////////

    printf("Add 6, 7\n");
    val = 6;
    splb_deque_PushBack(&deque, &val);
    val = 7;
    splb_deque_PushBack(&deque, &val);

    printf("Size: %zd | Empty: %d\n", splb_deque_Size(&deque), splb_deque_Empty(&deque));
    PrintList(&deque);
    putc('\n', stdout);
    ////////////////////////////////////////////////////////////////////////////

    printf("Add 4\n");
    val = 4;
    splb_deque_Insert(&deque, splb_deque_At(&deque, 2), &val);

    printf("Size: %zd | Empty: %d\n", splb_deque_Size(&deque), splb_deque_Empty(&deque));
    PrintList(&deque);
    putc('\n', stdout);
    ////////////////////////////////////////////////////////////////////////////

    printf("Add 2\n");
    val = 2;
    splb_deque_Insert(&deque, splb_deque_Begin(&deque), &val);

    printf("Size: %zd | Empty: %d\n", splb_deque_Size(&deque), splb_deque_Empty(&deque));
    PrintList(&deque);
    putc('\n', stdout);
    ////////////////////////////////////////////////////////////////////////////

    printf("Delete 3\n");
    splb_deque_EraseIt(&deque, splb_deque_At(&deque, 2));

    printf("Size: %zd | Empty: %d\n", splb_deque_Size(&deque), splb_deque_Empty(&deque));
    PrintList(&deque);
    putc('\n', stdout);
    ////////////////////////////////////////////////////////////////////////////

    splb_deque_DeInit(&deque);
}

void test_2()
{
    splb_Deque_t deque;
    splb_deque_Init(&deque);

    puts("Initialised");

    printf("Size: %zd | Empty: %d\n", splb_deque_Size(&deque), splb_deque_Empty(&deque));

    splb_deque_ValueType_t val = 11;
    splb_deque_PushFront(&deque, &val);

    PrintList(&deque);
    puts("");

    splb_deque_DeInit(&deque);
}

void test_3()
{
    splb_Deque_t deque;

    getc(stdin);

    splb_deque_InitN(&deque, 1000000000); // 256000000LU

    getc(stdin);

    splb_deque_DeInit(&deque);

    getc(stdin);
}

int main()
{
    // test_1();
    // test_2();
    test_3();
    return 0;
}
