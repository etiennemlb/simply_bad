//
//    @file deque.c
//    @author Etienne Malaboeuf
//    @brief
//    @version 0.1
//    @date 2020-03-04
//
//    Copyright 2020 Etienne Malaboeuf

//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at

//        http://www.apache.org/licenses/LICENSE-2.0

//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//

#include <SPLB/deque.h>
#include <stdlib.h>
#include <string.h>

////////////////////////////////////////////////////////////////////////////////
// Iterator
////////////////////////////////////////////////////////////////////////////////

// Private.

void splb_deque_IteratorSetSubarray(splb_deque_Iterator_t* it, splb_deque_ValueType_t** current_sub_array);

splb_deque_Iterator_t splb_deque_IteratorCopy(splb_deque_Iterator_t to_begin,
                                              splb_deque_Iterator_t from_begin,
                                              splb_deque_Iterator_t from_end);

splb_deque_Iterator_t splb_deque_IteratorCopyBackward(splb_deque_Iterator_t to_end,
                                                      splb_deque_Iterator_t from_begin,
                                                      splb_deque_Iterator_t from_end);

// Public.

splb_deque_ValueType_t* splb_deque_IteratorGet(const splb_deque_Iterator_t* it)
{
    return it->current_;
}

splb_deque_Iterator_t* splb_deque_IteratorIncrement(splb_deque_Iterator_t* it)
{
    if(++it->current_ == it->end_)
    {
        it->begin_   = *++it->current_sub_array_ptr_;
        it->end_     = it->begin_ + SPL_DEQUE_DEFAULT_SUBARRAY_SIZE;
        it->current_ = it->begin_;
    }

    return it;
}

splb_deque_Iterator_t* splb_deque_IteratorDecrement(splb_deque_Iterator_t* it)
{
    if(it->current_ == it->begin_)
    {
        it->begin_   = *--it->current_sub_array_ptr_;
        it->end_     = it->begin_ + SPL_DEQUE_DEFAULT_SUBARRAY_SIZE;
        it->current_ = it->end_;
    }

    --it->current_;
    return it;
}

splb_deque_Iterator_t* splb_deque_IteratorAdd(splb_deque_Iterator_t* it, splb_deque_DifferenceType_t n)
{
    const splb_deque_DifferenceType_t sub_array_pos = (it->current_ - it->begin_) + n;

    if((size_t)sub_array_pos < (size_t)splb_deque_IteratorSetSubarray)
    {
        it->current_ += n;
    }
    else
    {
        // EASTL :
        // This implementation is a branchless version which works by offsetting
        // the math to always be in the positive range. Much of the values here
        // reduce to constants and both the multiplication and division are of
        // power of two sizes and so this calculation ends up compiling down to
        // just one addition, one shift and one subtraction. This algorithm has
        // a theoretical weakness in that on 32 bit systems it will fail if the
        // value of n is >= (2^32 - 2^24) or 4,278,190,080 of if kDequeSubarraySize
        // is >= 2^24 or 16,777,216.
        const splb_deque_DifferenceType_t sub_array_idx =
            (((16777216 + sub_array_pos) / (splb_deque_DifferenceType_t)SPL_DEQUE_DEFAULT_SUBARRAY_SIZE)) -
            (16777216 / (splb_deque_DifferenceType_t)SPL_DEQUE_DEFAULT_SUBARRAY_SIZE);

        splb_deque_IteratorSetSubarray(it, it->current_sub_array_ptr_ + sub_array_idx);
        it->current_ = it->begin_ +
                       (sub_array_pos - (sub_array_idx * (splb_deque_DifferenceType_t)SPL_DEQUE_DEFAULT_SUBARRAY_SIZE));
    }
    return it;
}

splb_deque_Iterator_t* splb_deque_IteratorMinus(splb_deque_Iterator_t* it, splb_deque_DifferenceType_t n)
{
    return splb_deque_IteratorAdd(it, -n);
}

int splb_deque_IteratorEquals(const splb_deque_Iterator_t* a, const splb_deque_Iterator_t* b)
{
    return a->current_ == b->current_;
}

int splb_deque_IteratorNotEquals(const splb_deque_Iterator_t* a, const splb_deque_Iterator_t* b)
{
    return a->current_ != b->current_;
}

int splb_deque_IteratorSupp(const splb_deque_Iterator_t* a, const splb_deque_Iterator_t* b)
{
    return (a->current_sub_array_ptr_ == b->current_sub_array_ptr_) ?
               (a->current_ > b->current_) :
               (a->current_sub_array_ptr_ > b->current_sub_array_ptr_);
}

int splb_deque_IteratorLess(const splb_deque_Iterator_t* a, const splb_deque_Iterator_t* b)
{
    return (a->current_sub_array_ptr_ == b->current_sub_array_ptr_) ?
               (a->current_ < b->current_) :
               (a->current_sub_array_ptr_ < b->current_sub_array_ptr_);
}

int splb_deque_IteratorLessOrEquals(const splb_deque_Iterator_t* a, const splb_deque_Iterator_t* b)
{
    return (a->current_sub_array_ptr_ == b->current_sub_array_ptr_) ?
               (a->current_ <= b->current_) :
               (a->current_sub_array_ptr_ <= b->current_sub_array_ptr_);
}

int splb_deque_IteratorSuppOrEquals(const splb_deque_Iterator_t* a, const splb_deque_Iterator_t* b)
{
    return (a->current_sub_array_ptr_ == b->current_sub_array_ptr_) ?
               (a->current_ >= b->current_) :
               (a->current_sub_array_ptr_ >= b->current_sub_array_ptr_);
}

splb_deque_DifferenceType_t splb_deque_IteratorDistance(const splb_deque_Iterator_t* a, const splb_deque_Iterator_t* b)
{
    // used in deque since the hp stl.
    return ((splb_deque_DifferenceType_t)SPL_DEQUE_DEFAULT_SUBARRAY_SIZE *
            ((a->current_sub_array_ptr_ - b->current_sub_array_ptr_) - 1)) +
           (a->current_ - a->begin_) + (b->end_ - b->current_);
}

// Private Implementation.

void splb_deque_IteratorSetSubarray(splb_deque_Iterator_t* it, splb_deque_ValueType_t** current_sub_array)
{
    it->current_sub_array_ptr_ = current_sub_array;
    it->begin_                 = *current_sub_array;
    it->end_                   = it->begin_ + SPL_DEQUE_DEFAULT_SUBARRAY_SIZE;
}

splb_deque_Iterator_t splb_deque_IteratorCopy(splb_deque_Iterator_t to_begin,
                                              splb_deque_Iterator_t from_begin,
                                              splb_deque_Iterator_t from_end)
{
    for(; splb_deque_IteratorNotEquals(&from_begin, &from_end);
        splb_deque_IteratorIncrement(&to_begin), splb_deque_IteratorIncrement(&from_begin))
    {
        *splb_deque_IteratorGet(&to_begin) = *splb_deque_IteratorGet(&from_begin);
    }

    return to_begin;
}

splb_deque_Iterator_t splb_deque_IteratorCopyBackward(splb_deque_Iterator_t to_end,
                                                      splb_deque_Iterator_t from_begin,
                                                      splb_deque_Iterator_t from_end)
{
    for(splb_deque_DifferenceType_t n = splb_deque_IteratorDistance(&from_end, &from_begin); n > 0; --n)
    {
        *splb_deque_IteratorGet(splb_deque_IteratorDecrement(&to_end)) =
            *splb_deque_IteratorGet(splb_deque_IteratorDecrement(&from_end));
    }

    return to_end;
}

////////////////////////////////////////////////////////////////////////////////
// Deque base
////////////////////////////////////////////////////////////////////////////////

// Private.

enum Side
{
    ENUM_SIDE_FRONT,
    ENUM_SIDE_BACK
};

// Do not call that its fucked up.
int splb_deque_BaseInit_(splb_deque_Base_t* base);
int splb_deque_BaseInitN(splb_deque_Base_t* base, splb_deque_SizeType_t n);
int splb_deque_BaseDeInit(splb_deque_Base_t* base);

splb_deque_ValueType_t* splb_deque_BaseAllocateSubArray();
void                    splb_deque_BaseFreeSubArray(splb_deque_ValueType_t* array);
void                    splb_deque_BaseFreeSubArrays(splb_deque_ValueType_t** begin, splb_deque_ValueType_t** end);

splb_deque_ValueType_t** splb_deque_BaseAllocatePtrArray(splb_deque_SizeType_t n);
void                     splb_deque_BaseFreePtrArray(splb_deque_ValueType_t** ptr);

void splb_deque_BaseReallocPtrArray(splb_deque_Base_t* base, splb_deque_SizeType_t capacity_to_add, enum Side side);

// Public.

// Private Implementation.

int splb_deque_BaseInit_(splb_deque_Base_t* base)
{
    if(base == NULL)
    {
        return -1;
    }

    base->ptr_array_      = NULL;
    base->ptr_array_size_ = 0;

    memset(&base->it_begin_, 0, sizeof(splb_deque_Iterator_t));
    memset(&base->it_end_, 0, sizeof(splb_deque_Iterator_t));

    return 0;
}

int splb_deque_BaseInitN(splb_deque_Base_t* base, splb_deque_SizeType_t n)
{
    if(splb_deque_BaseInit_(base) < 0)
    {
        return -1;
    }

    // init n.
    {
        const splb_deque_SizeType_t new_ptr_array_size =
            (splb_deque_SizeType_t)((n / SPL_DEQUE_DEFAULT_SUBARRAY_SIZE) + 1);

        base->ptr_array_size_ = new_ptr_array_size > 8 ? new_ptr_array_size : 8;
        base->ptr_array_      = splb_deque_BaseAllocatePtrArray(base->ptr_array_size_);

        // compiler should optimize / 2 as >> 1
        splb_deque_ValueType_t** ptr_array_first =
            (base->ptr_array_ + ((base->ptr_array_size_ - new_ptr_array_size) / 2));
        splb_deque_ValueType_t** ptr_array_end     = ptr_array_first + new_ptr_array_size;
        splb_deque_ValueType_t** ptr_array_current = ptr_array_first;

        while(ptr_array_current < ptr_array_end)
        {
            splb_deque_ValueType_t* new_sub_array;

            if((new_sub_array = splb_deque_BaseAllocateSubArray()) == NULL)
            {
                splb_deque_BaseFreeSubArrays(ptr_array_first, ptr_array_current);

                splb_deque_BaseFreePtrArray(base->ptr_array_);

                base->ptr_array_      = NULL;
                base->ptr_array_size_ = 0;
                return -1;
            }

            *ptr_array_current = new_sub_array;
            ++ptr_array_current;
        }

        splb_deque_IteratorSetSubarray(&base->it_begin_, ptr_array_first);
        base->it_begin_.current_ = base->it_begin_.begin_;

        splb_deque_IteratorSetSubarray(&base->it_end_, ptr_array_end - 1);
        base->it_end_.current_ =
            base->it_end_.begin_ + (splb_deque_DifferenceType_t)(n % SPL_DEQUE_DEFAULT_SUBARRAY_SIZE);
    }
    return 0;
}

int splb_deque_BaseDeInit(splb_deque_Base_t* base)
{
    if(base == NULL)
    {
        return -1;
    }

    if(base->ptr_array_ != NULL)
    {
        splb_deque_BaseFreeSubArrays(base->it_begin_.current_sub_array_ptr_, base->it_end_.current_sub_array_ptr_ + 1);
        splb_deque_BaseFreePtrArray(base->ptr_array_);
    }

    return 0;
}

splb_deque_ValueType_t* splb_deque_BaseAllocateSubArray()
{
    return (splb_deque_ValueType_t*)malloc(SPL_DEQUE_DEFAULT_SUBARRAY_SIZE * sizeof(splb_deque_ValueType_t));
}

void splb_deque_BaseFreeSubArray(splb_deque_ValueType_t* array)
{
    if(array != NULL)
    {
        free(array);
        array = NULL;
    }
}

void splb_deque_BaseFreeSubArrays(splb_deque_ValueType_t** begin, splb_deque_ValueType_t** end)
{
    while(begin < end)
    {
        splb_deque_BaseFreeSubArray(*begin);
        ++begin;
    }
}

splb_deque_ValueType_t** splb_deque_BaseAllocatePtrArray(splb_deque_SizeType_t n)
{
    return (splb_deque_ValueType_t**)malloc(n * sizeof(splb_deque_ValueType_t*));
}

void splb_deque_BaseFreePtrArray(splb_deque_ValueType_t** ptr)
{
    if(ptr != NULL)
    {
        free(ptr);
    }
}

void splb_deque_BaseReallocPtrArray(splb_deque_Base_t* base, splb_deque_SizeType_t capacity_to_add, enum Side side)
{
    const splb_deque_SizeType_t unused_arrays_at_front =
        (splb_deque_SizeType_t)(base->it_begin_.current_sub_array_ptr_ - base->ptr_array_);
    const splb_deque_SizeType_t used_arrays =
        (splb_deque_SizeType_t)(base->it_end_.current_sub_array_ptr_ - base->it_begin_.current_sub_array_ptr_) + 1;
    const splb_deque_SizeType_t used_ptr_array_space  = used_arrays * sizeof(void*);
    const splb_deque_SizeType_t unused_arrays_at_back = base->ptr_array_size_ - unused_arrays_at_front - used_arrays;

    splb_deque_ValueType_t** ptr_array;

    // instead of reallocating all the time, shift left
    if((side == ENUM_SIDE_BACK) && (capacity_to_add <= unused_arrays_at_front))
    {
        // larger offset if it happens that we have lots of space in the front
        if(capacity_to_add < (unused_arrays_at_front / 2))  // compiler should optimize / 2 as >> 1
            capacity_to_add = (unused_arrays_at_front / 2); // compiler should optimize / 2 as >> 1

        ptr_array = base->ptr_array_ + (unused_arrays_at_front - capacity_to_add);
        memmove(ptr_array, base->it_begin_.current_sub_array_ptr_, used_ptr_array_space);
    }
    // instead of reallocating all the time, shift right
    else if((side == ENUM_SIDE_FRONT) && (capacity_to_add <= unused_arrays_at_back))
    {
        // larger offset if it happens that we have lots of space in the back
        if(capacity_to_add < (unused_arrays_at_back / 2))  // compiler should optimize / 2 as >> 1
            capacity_to_add = (unused_arrays_at_back / 2); // compiler should optimize / 2 as >> 1

        ptr_array = base->ptr_array_ + capacity_to_add;
        memmove(ptr_array, base->it_begin_.current_sub_array_ptr_, used_ptr_array_space);
    }
    else
    {
        // Else, reallocating memory. We increase the ptr_array size to be able to old more sub arrays.
        // then we recenter the used used ptr.
        const splb_deque_SizeType_t new_ptr_array_size =
            base->ptr_array_size_ +
            (base->ptr_array_size_ > capacity_to_add ? base->ptr_array_size_ : capacity_to_add) + 2; // 2x + 2
        splb_deque_ValueType_t** new_ptr_array = splb_deque_BaseAllocatePtrArray(new_ptr_array_size);

        ptr_array = new_ptr_array + (base->it_begin_.current_sub_array_ptr_ - base->ptr_array_) +
                    ((side == ENUM_SIDE_FRONT) ? capacity_to_add : 0);

        if(base->ptr_array_ != NULL)
            memcpy(ptr_array, base->it_begin_.current_sub_array_ptr_, used_ptr_array_space);

        splb_deque_BaseFreePtrArray(base->ptr_array_);

        base->ptr_array_      = new_ptr_array;
        base->ptr_array_size_ = new_ptr_array_size;
    }

    splb_deque_IteratorSetSubarray(&base->it_begin_, ptr_array);
    splb_deque_IteratorSetSubarray(&base->it_end_, (ptr_array + used_arrays) - 1);
}

////////////////////////////////////////////////////////////////////////////////
// Deque
////////////////////////////////////////////////////////////////////////////////

// Private.

// Public.

int splb_deque_Init(splb_Deque_t* deque)
{
    return splb_deque_InitN(deque, 0);
}

int splb_deque_InitN(splb_Deque_t* deque, splb_deque_SizeType_t n)
{
    return splb_deque_BaseInitN(&deque->base, n);
}

void splb_deque_DeInit(splb_Deque_t* deque)
{
    splb_deque_BaseDeInit(&deque->base);
}

// int splb_deque_AssignN(splb_Deque_t* deque,
//                          splb_deque_SizeType_t n,
//                          const splb_deque_ValueType_t* value);

// int splb_deque_AssignFromArray(splb_Deque_t* deque,
//                                  splb_deque_SizeType_t array_size,
//                                  const splb_deque_ValueType_t* array);

splb_deque_Iterator_t splb_deque_Begin(const splb_Deque_t* deque)
{
    return deque->base.it_begin_;
}

splb_deque_Iterator_t splb_deque_End(const splb_Deque_t* deque)
{
    return deque->base.it_end_;
}

// splb_deque_Iterator_t splb_deque_RBegin(splb_Deque_t* deque);
// splb_deque_Iterator_t splb_deque_REnd(splb_Deque_t* deque);

int splb_deque_Empty(const splb_Deque_t* deque)
{
    return deque->base.it_begin_.current_ == deque->base.it_end_.current_;
}

splb_deque_SizeType_t splb_deque_Size(const splb_Deque_t* deque)
{
    return splb_deque_IteratorDistance(&deque->base.it_end_, &deque->base.it_begin_);
}

// int splb_deque_ResizeWithVal(splb_Deque_t* deque,
//                                splb_deque_SizeType_t n,
//                                const splb_deque_ValueType_t* value);
// int splb_deque_Resize(splb_Deque_t* deque,
//                         splb_deque_SizeType_t n);

// int splb_deque_ShrinkToFit(splb_Deque_t* deque);
// int splb_deque_SetCapacity(splb_Deque_t* deque,
//                              splb_deque_SizeType_t n);

splb_deque_Iterator_t splb_deque_At(const splb_Deque_t* deque, splb_deque_SizeType_t n)
{
    splb_deque_Iterator_t it = deque->base.it_begin_;
    splb_deque_IteratorAdd(&it, (splb_deque_DifferenceType_t)n);
    return it;
}

splb_deque_ValueType_t* splb_deque_Get(const splb_Deque_t* deque, splb_deque_SizeType_t n)
{
    splb_deque_Iterator_t it = deque->base.it_begin_;

    const splb_deque_DifferenceType_t sub_array_pos = (it.current_ - it.begin_) + (splb_deque_DifferenceType_t)n;

    const splb_deque_DifferenceType_t sub_array_idx =
        (((16777216 + sub_array_pos) / (splb_deque_DifferenceType_t)SPL_DEQUE_DEFAULT_SUBARRAY_SIZE)) -
        (16777216 / (splb_deque_DifferenceType_t)SPL_DEQUE_DEFAULT_SUBARRAY_SIZE);

    // No branches for better branch prediction.
    
    // Expended vection of return splb_deque_IteratorGet(
    //                                                         splb_deque_At(deque, n)
    //                                                     );
    
    return (*(it.current_sub_array_ptr_ + sub_array_idx) +
            (sub_array_pos - (sub_array_idx * (splb_deque_DifferenceType_t)SPL_DEQUE_DEFAULT_SUBARRAY_SIZE)));
}

splb_deque_ValueType_t* splb_deque_Front(const splb_Deque_t* deque)
{
    return deque->base.it_begin_.current_;
}

splb_deque_ValueType_t* splb_deque_Back(const splb_Deque_t* deque)
{
    splb_deque_Iterator_t it = deque->base.it_end_;
    splb_deque_IteratorDecrement(&it);
    return it.current_;
}

int splb_deque_PushFront(splb_Deque_t* deque, const splb_deque_ValueType_t* value)
{
    if(deque->base.it_begin_.begin_ != deque->base.it_begin_.current_)
    {
        *(--deque->base.it_begin_.current_) = *value;
    }
    else
    {
        splb_deque_ValueType_t saved_value = *value;
        if(deque->base.it_begin_.current_sub_array_ptr_ == deque->base.ptr_array_)
        {
            splb_deque_BaseReallocPtrArray(&deque->base, 1, ENUM_SIDE_FRONT);
        }

        deque->base.it_begin_.current_sub_array_ptr_[-1] = splb_deque_BaseAllocateSubArray();

        if(deque->base.it_begin_.current_sub_array_ptr_[-1] == NULL)
        {
            splb_deque_BaseFreeSubArray(deque->base.it_begin_.current_sub_array_ptr_[-1]);
            return -1;
        }

        splb_deque_IteratorSetSubarray(&deque->base.it_begin_, deque->base.it_begin_.current_sub_array_ptr_ - 1);
        deque->base.it_begin_.current_ = deque->base.it_begin_.end_ - 1;

        *deque->base.it_begin_.current_ = saved_value;
    }
    return 0;
}

int splb_deque_PushBack(splb_Deque_t* deque, const splb_deque_ValueType_t* value)
{
    if(deque->base.it_end_.end_ !=
       (deque->base.it_end_.current_ + 1)) // bad (deque->base.it_end_.end_ - 1 != deque->base.it_end_.current_)
    {
        *(deque->base.it_end_.current_++) = *value;
    }
    else
    {
        splb_deque_ValueType_t saved_value = *value;
        if(((deque->base.it_end_.current_sub_array_ptr_ - deque->base.ptr_array_) + 1) >=
           (splb_deque_DifferenceType_t)deque->base.ptr_array_size_)
        {
            splb_deque_BaseReallocPtrArray(&deque->base, 1, ENUM_SIDE_BACK);
        }

        deque->base.it_end_.current_sub_array_ptr_[1] = splb_deque_BaseAllocateSubArray();

        if(deque->base.it_end_.current_sub_array_ptr_[1] == NULL)
        {
            splb_deque_BaseFreeSubArray(deque->base.it_end_.current_sub_array_ptr_[1]);
            return -1;
        }

        splb_deque_IteratorSetSubarray(&deque->base.it_end_, deque->base.it_end_.current_sub_array_ptr_ + 1);
        *deque->base.it_end_.current_ = saved_value;
    }
    return 0;
}

int splb_deque_PopFront(splb_Deque_t* deque)
{
    if((deque->base.it_begin_.current_ + 1) != deque->base.it_begin_.end_)
    {
        ++deque->base.it_begin_.current_;
    }
    else
    {
        splb_deque_BaseFreeSubArray(deque->base.it_begin_.begin_);

        splb_deque_IteratorSetSubarray(&deque->base.it_begin_, deque->base.it_begin_.current_sub_array_ptr_ + 1);
        deque->base.it_begin_.current_ = deque->base.it_begin_.begin_;
    }
    return 0;
}

int splb_deque_PopBack(splb_Deque_t* deque)
{
    if(deque->base.it_end_.current_ != deque->base.it_end_.begin_)
    {
        --deque->base.it_end_.current_;
    }
    else
    {
        splb_deque_BaseFreeSubArray(deque->base.it_end_.begin_);

        splb_deque_IteratorSetSubarray(&deque->base.it_end_, deque->base.it_end_.current_sub_array_ptr_ - 1);
        deque->base.it_end_.current_ = deque->base.it_end_.end_ - 1;
    }
    return 0;
}

splb_deque_Iterator_t splb_deque_Insert(splb_Deque_t*                 deque,
                                        splb_deque_Iterator_t         pos,
                                        const splb_deque_ValueType_t* value)
{
    if(pos.current_ == deque->base.it_end_.current_)
    {
        splb_deque_PushBack(deque, value);
        splb_deque_Iterator_t it = deque->base.it_end_;
        return it;
    }
    else if(pos.current_ == deque->base.it_begin_.current_)
    {
        splb_deque_PushFront(deque, value);
        return deque->base.it_begin_;
    }

    splb_deque_Iterator_t             it = pos;
    const splb_deque_DifferenceType_t pos_relative_to_begin =
        (splb_deque_IteratorDistance(&it, &deque->base.it_begin_));
    splb_deque_ValueType_t saved_value = *value;

    // compiler should optimize / 2 as >> 1.
    if(pos_relative_to_begin <
       (splb_deque_DifferenceType_t)(splb_deque_Size(deque) / 2)) // which side are we closer to.
    {
        splb_deque_PushFront(deque, deque->base.it_begin_.current_);

        it = deque->base.it_begin_;
        splb_deque_IteratorAdd(&it, pos_relative_to_begin);

        splb_deque_Iterator_t new_pos = it;
        splb_deque_IteratorIncrement(&new_pos);
        splb_deque_Iterator_t old_begin = deque->base.it_begin_;
        splb_deque_IteratorIncrement(&old_begin);
        splb_deque_Iterator_t old_begin_plus1 = old_begin;
        splb_deque_IteratorIncrement(&old_begin_plus1);

        splb_deque_IteratorCopy(old_begin, old_begin_plus1, new_pos);
    }
    else
    {
        splb_deque_Iterator_t end = deque->base.it_end_;
        splb_deque_PushBack(deque, splb_deque_IteratorGet(splb_deque_IteratorDecrement(&end)));

        it = deque->base.it_begin_;
        splb_deque_IteratorAdd(&it, pos_relative_to_begin);

        splb_deque_Iterator_t old_back = deque->base.it_end_;
        splb_deque_IteratorDecrement(&old_back);
        splb_deque_Iterator_t old_back_minus1 = old_back;
        splb_deque_IteratorDecrement(&old_back_minus1);

        splb_deque_IteratorCopyBackward(old_back, it, old_back_minus1);
    }

    *splb_deque_IteratorGet(&it) = saved_value;
    return it;
}

// int splb_deque_InsertN(splb_Deque_t* deque,
//                          splb_deque_Iterator_t pos,
//                          splb_deque_SizeType_t n,
//                          const splb_deque_ValueType_t* value);
// int splb_deque_InsertFromArray(splb_Deque_t* deque,
//                                  splb_deque_Iterator_t pos,
//                                  splb_deque_SizeType_t array_size,
//                                  const splb_deque_ValueType_t* array);

splb_deque_Iterator_t splb_deque_EraseIt(splb_Deque_t* deque, splb_deque_Iterator_t pos)
{
    splb_deque_Iterator_t it_next = pos;
    splb_deque_IteratorIncrement(&it_next);

    const splb_deque_DifferenceType_t pos_relative_to_begin = splb_deque_IteratorDistance(&pos, &deque->base.it_begin_);

    if(pos_relative_to_begin <
       (splb_deque_DifferenceType_t)(splb_deque_Size(deque) / 2)) // compiler should optimise with >> 1
    {
        splb_deque_IteratorCopyBackward(it_next, deque->base.it_begin_, pos);
        splb_deque_PopFront(deque);
    }
    else
    {
        splb_deque_IteratorCopy(pos, it_next, deque->base.it_end_);
        splb_deque_PopBack(deque);
    }

    splb_deque_Iterator_t ret = deque->base.it_begin_;
    splb_deque_IteratorAdd(&ret, pos_relative_to_begin);
    return ret;
}

// int splb_deque_EraseRange(splb_Deque_t* deque,
//                             splb_deque_Iterator_t first,
//                             splb_deque_Iterator_t last);

void splb_deque_Clear(splb_Deque_t* deque)
{
    if(deque->base.it_begin_.current_sub_array_ptr_ != deque->base.it_end_.current_sub_array_ptr_)
    {
        splb_deque_BaseFreeSubArray(deque->base.it_end_.begin_);
    }

    for(splb_deque_ValueType_t** ptr_array = deque->base.it_begin_.current_sub_array_ptr_ + 1;
        ptr_array < deque->base.it_end_.current_sub_array_ptr_;
        ++ptr_array)
    {
        splb_deque_BaseFreeSubArray(*ptr_array);
    }

    deque->base.it_end_ = deque->base.it_begin_;
}
