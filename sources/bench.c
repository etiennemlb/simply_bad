//
//    @file bench.c
//    @author Etienne Malaboeuf
//    @brief
//    @version 0.1
//    @date 2020-03-04
//
//    Copyright 2020 Etienne Malaboeuf

//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at

//        http://www.apache.org/licenses/LICENSE-2.0

//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//

#include <SPLB/bench.h>

#if defined(SPL_OS_IS_WINDOWS)
    #include <Windows.h>
#elif defined(SPL_OS_IS_UNIX)
    #include <time.h>
#endif

splb_TimeNs_t splb_bench_GetCurrentTimeNs()
{
#if defined(SPL_OS_IS_WINDOWS)
    static LARGE_INTEGER clock_freq = {0};
    LARGE_INTEGER        current_tick_count;

    if(clock_freq.QuadPart == 0)
    {
        QueryPerformanceFrequency(&clock_freq);
    }

    QueryPerformanceCounter(&current_tick_count);

    // We now have the elapsed number of ticks, along with the
    // number of ticks-per-second. We use these values
    // to convert to the number of elapsed microseconds.
    // To guard against loss-of-precision, we convert
    // to microseconds *before* dividing by ticks-per-second.
    // cf https://docs.microsoft.com/en-us/windows/win32/sysinfo/acquiring-high-resolution-time-stamps

    current_tick_count.QuadPart *= (1000 * 1000 * 1000); // we actualy need nano sec.
    current_tick_count.QuadPart /= clock_freq.QuadPart;

    return (splb_TimeNs_t)current_tick_count.QuadPart;
#elif defined(SPL_OS_IS_UNIX)
    struct timespec now;
    clock_gettime(SPL_DEFAULT_CLOCK_TYPE, &now);

    return now.tv_sec * 1000000000 + now.tv_nsec;
#endif
}
