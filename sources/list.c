
//
//    @file list.c
//    @author Etienne Malaboeuf
//    @brief
//    @version 0.1
//    @date 2020-03-04
//
//    Copyright 2020 Etienne Malaboeuf

//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at

//        http://www.apache.org/licenses/LICENSE-2.0

//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//

#include <SPLB/list.h>
#include <stdlib.h>

////////////////////////////////////////////////////////////////////////////////
// Node base private
////////////////////////////////////////////////////////////////////////////////

// Private declaration.

void splb_list_NodeBaseInsert(splb_list_Node_t* it, splb_list_Node_t* to_add);
void splb_list_NodeBaseRemove(splb_list_Node_t* it);

void splb_list_NodeBaseReverse(splb_list_Node_t* it);

void splb_list_NodeBaseInsertRange(splb_list_Node_t* it, splb_list_Node_t* first, splb_list_Node_t* last);
void splb_list_NodeBaseRemoveRange(splb_list_Node_t* first, splb_list_Node_t* last);

// Private implementation.

void splb_list_NodeBaseInsert(splb_list_Node_t* it, splb_list_Node_t* to_add)
{
    to_add->next_    = it;
    to_add->prev_    = it->prev_;
    it->prev_->next_ = to_add;
    it->prev_        = to_add;
}

void splb_list_NodeBaseRemove(splb_list_Node_t* it)
{
    it->prev_->next_ = it->next_;
    it->next_->prev_ = it->prev_;
}

void splb_list_NodeBaseReverse(splb_list_Node_t* it)
{
    splb_list_Node_t* current_node_ = it;
    do
    {
        splb_list_Node_t* tmp = current_node_->next_;
        current_node_->next_  = current_node_->prev_;
        current_node_->prev_  = tmp;
        current_node_         = current_node_->prev_;
    } while(current_node_ != it);
}

void splb_list_NodeBaseInsertRange(splb_list_Node_t* it, splb_list_Node_t* first, splb_list_Node_t* last)
{
    it->prev_->next_ = first;
    first->prev_     = it->prev_;
    it->prev_        = last;
    last->next_      = it;
}

void splb_list_NodeBaseRemoveRange(splb_list_Node_t* first, splb_list_Node_t* last)
{
    first->prev_->next_ = last->next_;
    last->next_->prev_  = first->prev_;
}

////////////////////////////////////////////////////////////////////////////////
// Iterator
////////////////////////////////////////////////////////////////////////////////

// Public implementation.

splb_list_ValueType_t* splb_list_IteratorGet(const splb_list_Iterator_t* it)
{
    return &it->current_node_->value_;
}

splb_list_Iterator_t splb_list_IteratorNext(splb_list_Iterator_t* it)
{
    splb_list_Iterator_t next = {it->current_node_->next_};
    return next;
}

splb_list_Iterator_t splb_list_IteratorPrev(splb_list_Iterator_t* it)
{
    splb_list_Iterator_t prev = {it->current_node_->prev_};
    return prev;
}

splb_list_Iterator_t* splb_list_IteratorIncrement(splb_list_Iterator_t* it)
{
    it->current_node_ = it->current_node_->next_;
    return it;
}

splb_list_Iterator_t* splb_list_IteratorDecrement(splb_list_Iterator_t* it)
{
    it->current_node_ = it->current_node_->prev_;
    return it;
}

int splb_list_IteratorEquals(const splb_list_Iterator_t* a, const splb_list_Iterator_t* b)
{
    return a->current_node_ == b->current_node_;
}

int splb_list_IteratorNotEquals(const splb_list_Iterator_t* a, const splb_list_Iterator_t* b)
{
    return a->current_node_ != b->current_node_;
}

splb_list_DifferenceType_t splb_list_IteratorDistance(const splb_list_Iterator_t* first,
                                                      const splb_list_Iterator_t* last)
{
    splb_DifferenceType_t n  = 0;
    splb_list_Iterator_t  it = *first;
    while(splb_list_IteratorNotEquals(&it, last))
    {
        splb_list_IteratorIncrement(&it);
        ++n;
    }
    return n;
}

////////////////////////////////////////////////////////////////////////////////
// List base, private
////////////////////////////////////////////////////////////////////////////////

// Private declaration.

void splb_list_BaseInit(splb_list_Base_t* base);
void splb_list_BaseDeInit(splb_list_Base_t* base);

splb_list_Node_t* splb_list_BaseAllocateNode(splb_list_Base_t* base);
void              splb_list_BaseFreeNode(splb_list_Base_t* base, splb_list_Node_t* node);

void splb_list_BaseClear(splb_list_Base_t* base);

// Private implementation.

void splb_list_BaseInit(splb_list_Base_t* base)
{
    base->list_carrier_node_.next_ = &base->list_carrier_node_;
    base->list_carrier_node_.prev_ = &base->list_carrier_node_;
}

void splb_list_BaseDeInit(splb_list_Base_t* base)
{
    splb_list_BaseClear(base);
}

splb_list_Node_t* splb_list_BaseAllocateNode(splb_list_Base_t* base)
{
#if defined(SPL_LIST_USE_POOL)
    return (splb_list_Node_t*)splb_pool_Get(base->node_pool_);
#else
    SPL_UNUSED(base);
    return (splb_list_Node_t*)malloc(sizeof(splb_list_Node_t));
#endif
}

void splb_list_BaseFreeNode(splb_list_Base_t* base, splb_list_Node_t* node)
{
#if defined(SPL_LIST_USE_POOL)
    splb_pool_Release(base->node_pool_, node);
#else
    SPL_UNUSED(base);
    free(node);
#endif
}

void splb_list_BaseClear(splb_list_Base_t* base)
{
    splb_list_Node_t* current = base->list_carrier_node_.next_;

    while(current != &base->list_carrier_node_)
    {
        splb_list_Node_t* tmp = current;
        current               = current->next_;
        splb_list_BaseFreeNode(base, tmp);
    }
}

////////////////////////////////////////////////////////////////////////////////
// List
////////////////////////////////////////////////////////////////////////////////

// Private declaration.

void splb_list_InsertValue(splb_List_t* list, splb_list_Node_t* node, const splb_list_ValueType_t* value);
void splb_list_InsertValues(splb_List_t*                 list,
                            splb_list_Node_t*            node,
                            splb_list_SizeType_t         n,
                            const splb_list_ValueType_t* val);

void splb_list_EraseNode(splb_List_t* list, splb_list_Node_t* node)
{
    splb_list_NodeBaseRemove(node);
    splb_list_BaseFreeNode(&list->base_, node);
#if defined(SPL_LIST_CACHE_SIZE)
    --list->base_.size_;
#else
    SPL_UNUSED(list);
#endif
}

// Private implementation.

void splb_list_InsertValue(splb_List_t* list, splb_list_Node_t* node, const splb_list_ValueType_t* value)
{
    splb_list_Node_t* new_node = splb_list_BaseAllocateNode(&list->base_);
    new_node->value_           = *value;
    splb_list_NodeBaseInsert(node, new_node);
#if defined(SPL_LIST_CACHE_SIZE)
    ++list->base_.size_;
#else
    SPL_UNUSED(list);
#endif
}

void splb_list_InsertValues(splb_List_t*                 list,
                            splb_list_Node_t*            node,
                            splb_list_SizeType_t         n,
                            const splb_list_ValueType_t* value)
{
    while(n-- > 0)
    {
        splb_list_InsertValue(list, node, value);
    }
}

// Public implementation.

#if defined(SPL_LIST_USE_POOL)
int splb_list_Init(splb_List_t* list, splb_ObjectPool_t* pool)
#else
int splb_list_Init(splb_List_t* list)
#endif
{
#if defined(SPL_LIST_USE_POOL)
    if(splb_list_InitNVal(list, pool, 0, NULL) < 0)
#else
    if(splb_list_InitNVal(list, 0, NULL) < 0)
#endif
    {
        return -1;
    }

    return 0;
}

#if defined(SPL_LIST_USE_POOL)
int splb_list_InitNVal(splb_List_t*                 list,
                       splb_ObjectPool_t*           pool,
                       splb_list_SizeType_t         n,
                       const splb_list_ValueType_t* value)
#else
int splb_list_InitNVal(splb_List_t* list, splb_list_SizeType_t n, const splb_list_ValueType_t* value)
#endif
{
    if(list == NULL)
    {
        return -1;
    }

#if defined(SPL_LIST_CACHE_SIZE)
    list->base_.size_ = 0;
#endif

#if defined(SPL_LIST_USE_POOL)
    list->base_.node_pool_ = pool;
#endif

    splb_list_BaseInit(&list->base_);

    splb_list_InsertValues(list, &list->base_.list_carrier_node_, n, value);
    return 0;
}

void splb_list_DeInit(splb_List_t* list)
{
    splb_list_BaseDeInit(&list->base_);
}

splb_list_Iterator_t splb_list_Begin(const splb_List_t* list)
{
    splb_list_Iterator_t begin = {list->base_.list_carrier_node_.next_};
    return begin;
}

splb_list_Iterator_t splb_list_End(/* const */ splb_List_t* list)
{
    splb_list_Iterator_t end = {&list->base_.list_carrier_node_};
    return end;
}

int splb_list_Empty(const splb_List_t* list)
{
#if defined(SPL_LIST_CACHE_SIZE)
    return list->base_.size_ == 0;
#else
    return &list->base_.list_carrier_node_ == list->base_.list_carrier_node_.next_;
#endif
}

splb_list_SizeType_t splb_list_Size(/* const */ splb_List_t* list)
{
#if defined(SPL_LIST_CACHE_SIZE)
    return list->base_.size_;
#else
    const splb_list_Iterator_t first = {list->base_.list_carrier_node_.next_};
    const splb_list_Iterator_t last  = {&list->base_.list_carrier_node_};
    return splb_list_IteratorDistance(&first, &last);
#endif
}

splb_list_ValueType_t* splb_list_Front(const splb_List_t* list)
{
    return &list->base_.list_carrier_node_.next_->value_;
}

splb_list_ValueType_t* splb_list_Back(const splb_List_t* list)
{
    return &list->base_.list_carrier_node_.prev_->value_;
}

void splb_list_PushFront(splb_List_t* list, const splb_list_ValueType_t* value)
{
    splb_list_InsertValue(list, list->base_.list_carrier_node_.next_, value);
}

void splb_list_PushBack(splb_List_t* list, const splb_list_ValueType_t* value)
{
    splb_list_InsertValue(list, &list->base_.list_carrier_node_, value);
}

void splb_list_PopFront(splb_List_t* list)
{
    splb_list_EraseNode(list, list->base_.list_carrier_node_.next_);
}

void splb_list_PopBack(splb_List_t* list)
{
    splb_list_EraseNode(list, list->base_.list_carrier_node_.prev_);
}

splb_list_Iterator_t splb_list_Insert(splb_List_t* list, splb_list_Iterator_t pos, const splb_list_ValueType_t* value)
{
    splb_list_Node_t* new_node = splb_list_BaseAllocateNode(&list->base_);
    new_node->value_           = *value;
    splb_list_NodeBaseInsert(pos.current_node_, new_node);

    pos.current_node_ = new_node;
#if defined(SPL_LIST_CACHE_SIZE)
    ++list->base_.size_;
#else
    SPL_UNUSED(list);
#endif
    return pos;
}

splb_list_Iterator_t splb_list_EraseIt(splb_List_t* list, splb_list_Iterator_t pos)
{
    splb_list_IteratorIncrement(&pos);
    splb_list_EraseNode(list, pos.current_node_->prev_);
    return pos;
}

splb_list_Iterator_t splb_list_EraseRange(splb_List_t* list, splb_list_Iterator_t first, splb_list_Iterator_t last)
{
    while(splb_list_IteratorNotEquals(&first, &last))
    {
        first = splb_list_EraseIt(list, first);
    }

    splb_list_Iterator_t it = {last.current_node_};
    return it;
}

void splb_list_Clear(splb_List_t* list)
{
    splb_list_BaseClear(&list->base_);
    splb_list_BaseInit(&list->base_);
#if defined(SPL_LIST_CACHE_SIZE)
    list->base_.size_ = 0;
#endif
}

void splb_list_Reverse(splb_List_t* list)
{
    splb_list_NodeBaseReverse(&list->base_.list_carrier_node_);
}
