#include <SPLB/rand/xorshift128p.h>

int splb_rand_xorshift128p_InitState(splb_rand_xorshift128p_State_t* state, splb_RandU64_t seed)
{
    if(state == NULL)
    {
        return -1;
    }

    // thats bad !
    state->a = 0xDEADC0DEDEADC0DE ^ seed;
    state->b = ((0xB16B00B5BABE0000 ^ seed) >> (0xf & state->a)) | (state->a & 0xf << (64 - (0xf & state->a)));

    return 0;
}

// The state must be seeded so that it is not all zero.
splb_RandU64_t splb_rand_xorshift128p_NextU64(splb_rand_xorshift128p_State_t* state)
{
    splb_RandU64_t       t = state->a;
    splb_RandU64_t const s = state->b;
    state->a               = s;
    t ^= t << 23;       // a
    t ^= t >> 17;       // b
    t ^= s ^ (s >> 26); // c
    state->b = t;
    return t + s;
}
