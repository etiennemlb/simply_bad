//
//    @file pool.c
//    @author Etienne Malaboeuf
//    @brief
//    @version 0.1
//    @date 2020-03-04
//
//    Copyright 2020 Etienne Malaboeuf

//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at

//        http://www.apache.org/licenses/LICENSE-2.0

//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//

#include <SPLB/pool.h>
#include <stdlib.h>

////////////////////////////////////////////////////////////////////////////////
// Object Pool
////////////////////////////////////////////////////////////////////////////////

// Private.

struct splb_pool_Header_t
{
    char unused_; //!< 1 if unused, else 0.
};

#define SPL_POOL_HEADER_SIZE (sizeof(splb_pool_Header_t))

// Public implementation.

int splb_pool_Init(splb_ObjectPool_t*          pool,
                   splb_SizeType_t             count,
                   splb_SizeType_t             size_of_object,
                   sblp_pool_ObjectInitializer initializer)
{
    if(pool == NULL)
    {
        return -1;
    }

    if(count == 0)
    {
        return -1;
    }

    if((pool->headers_ = malloc(count * SPL_POOL_HEADER_SIZE)) == NULL)
    {
        return -1;
    }

    if((pool->mem_lo_ = malloc(count * size_of_object)) == NULL)
    {
        free(pool->headers_);
        return -1;
    }

    pool->block_size_  = size_of_object;
    pool->block_count_ = count;
    pool->initializer_ = initializer;

    // This loop forces the OS to initialize the pages.

    splb_pool_ResetUsage(pool);

    return 0;
}

splb_IdxType_t splb_pool_GetFirstFreeBlockBefore(const splb_ObjectPool_t* pool, splb_IdxType_t addr)
{
    splb_IdxType_t last_free_block = -1LL;
    splb_IdxType_t current         = addr - 1LL;

    while((current >= 0LL) && pool->headers_[current].unused_)
    {
        last_free_block = current;
        --current;
    }

    return last_free_block;
}

/**
 * @brief Get the first free block of the first free block aggregation starting from last.
 * TODO: bench for a reverse search. (start to last and not last to start)
 * Last is excluded from the search.
 *
 * @param pool
 * @param first
 * @param last
 * @return splb_IdxType_t
 */
splb_IdxType_t splb_pool_GetFreeBlockBetween(const splb_ObjectPool_t* pool, splb_IdxType_t first, splb_IdxType_t last)
{
    splb_IdxType_t current = last;

    while(first < current)
    {
        splb_IdxType_t free_found = splb_pool_GetFirstFreeBlockBefore(pool, current);
        if(free_found >= 0LL)
        {
            return free_found;
        }

        --current;
    }

    return -1LL;
}

// We should try to squeeze more perf out of this function.
void* splb_pool_Get(splb_ObjectPool_t* pool)
{
    if(pool == NULL)
    {
        return NULL;
    }

    splb_IdxType_t header_idx;
    splb_IdxType_t guess_idx = pool->last_free_block_found_;

    if(guess_idx < 0LL)
    {

        if((header_idx = splb_pool_GetFreeBlockBetween(pool, 0LL, pool->last_scan_free_block_found_)) < 0LL)
        {
            if((header_idx = splb_pool_GetFreeBlockBetween(
                    pool,
                    pool->last_scan_free_block_found_, // + pool->block_size_ (could overflow).
                    pool->block_count_)) < 0LL)
            {
                pool->last_scan_free_block_found_ = pool->block_count_;
                return NULL;
            }
        }
        pool->last_scan_free_block_found_ = header_idx; // - pool->block_size_; (could underflow).
    }
    else
    {
        if((header_idx = splb_pool_GetFirstFreeBlockBefore(pool, guess_idx)) < 0LL)
        {
            header_idx                   = guess_idx;
            pool->last_free_block_found_ = -1LL; // consumed
        }
    }

    pool->headers_[header_idx].unused_ = 0; // set the header to used.

    splb_IdxType_t next_header_idx = header_idx + 1LL;

    if(next_header_idx < pool->block_count_ && pool->headers_[next_header_idx].unused_)
    {
        pool->last_free_block_found_ = next_header_idx;
    }
    else
    {
        pool->last_free_block_found_ = -1LL;
    }

    return (char*)pool->mem_lo_ + header_idx * pool->block_size_;
}

void splb_pool_Release(splb_ObjectPool_t* pool, const void* mem)
{
    splb_IdxType_t idx = ((char*)mem - (char*)pool->mem_lo_) / pool->block_size_; // slow division. TODO, power of two blocksize maybe?

    // if(idx < 0)
    // {
    //     return;
    // }

    pool->last_free_block_found_ = idx;  // costly cuz of cache locality pbm
    pool->headers_[idx].unused_  = 1;
}

void splb_pool_DeInit(const splb_ObjectPool_t* pool)
{
    if(pool != NULL)
    {
        free(pool->mem_lo_);
        free(pool->headers_);
    }
}

void splb_pool_ResetUsage(splb_ObjectPool_t* pool)
{
    for(splb_IdxType_t i = 0LL; i < pool->block_count_; ++i)
    {
        pool->headers_[i].unused_ = 1; // set all flags to unused.
    }

    if(pool->initializer_ != NULL)
    {
        for(splb_IdxType_t i = 0LL; i < pool->block_count_; ++i)
        {
            pool->initializer_((char*)pool->mem_lo_ + i * pool->block_size_);
        }
    }
    // else
    // {
    //     for(splb_IdxType_t i = 0; i < pool->block_count_; ++i)
    //     {

    //         *((char*)pool->mem_lo_ + i * pool->block_size_) =
    //             1; // pre heating sorta. We force the os to truly increase the ws of the pool (program).
    //                // THIS IS CRUTIAL (or not), and need to prevent trash perf when first accessing a page.
    //     }
    // }

    pool->last_free_block_found_      = 0LL;
    pool->last_scan_free_block_found_ = pool->block_count_;
}
