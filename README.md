# Simply bad
Simply bad is simply a bad library implementing common datastructures. Its heavily inspired of the c++ stl and of it's interfaces. Also, many algorithms have been inspired of the EASTL.

## To compile you might need:
cmake

gcc > 4 or clang

## Compile:

$ mkdir build && cd build

$ cmake -DCMAKE_BUILD_TYPE=[Release|Debug] ..

$ cmake --build . -- -j 16 # or $ make -j 16

## Generate the doc

$ doxygen makedoc.doxy

The generated doc start at "docs/doxygen/html/index.html"
