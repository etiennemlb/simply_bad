//
//    @file pool.h
//    @author Etienne Malaboeuf
//    @brief
//    @version 0.1
//    @date 2020-03-04
//
//    Copyright 2020 Etienne Malaboeuf

//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at

//        http://www.apache.org/licenses/LICENSE-2.0

//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//

#ifndef SPL_OBJ_POOL_H
#define SPL_OBJ_POOL_H

#if defined(SPL_PRAGMA_ONCE_SUPPORTED)
    #pragma once // Some compilers (e.g. VC++) benefit significantly from using this. We've measured 3-4% build speed
                 // improvements in apps as a result. (EASTL)
#endif

#include <SPLB/internal/config.h>

typedef void (*sblp_pool_ObjectInitializer)(void* obj_mem);

typedef struct splb_pool_Header_t splb_pool_Header_t;

typedef struct
{

    splb_pool_Header_t*         headers_;
    void*                       mem_lo_;
    splb_IdxType_t              last_free_block_found_;
    splb_IdxType_t              last_scan_free_block_found_;
    splb_SizeType_t             block_size_;
    splb_IdxType_t              block_count_; // using a signed seems faster (on benchmarks).
    sblp_pool_ObjectInitializer initializer_;
} splb_ObjectPool_t;

int  splb_pool_Init(splb_ObjectPool_t*          pool,
                    splb_SizeType_t             count,
                    splb_SizeType_t             size_of_object,
                    sblp_pool_ObjectInitializer initializer);
void splb_pool_DeInit(const splb_ObjectPool_t* pool);

void* splb_pool_Get(splb_ObjectPool_t* pool);
void  splb_pool_Release(splb_ObjectPool_t* pool, const void* mem);

void splb_pool_ResetUsage(splb_ObjectPool_t* pool);

#endif
