//
//    @file debug.h
//    @author Etienne Malaboeuf
//    @brief
//    @version 0.1
//    @date 2020-03-04
//
//    Copyright 2020 Etienne Malaboeuf

//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at

//        http://www.apache.org/licenses/LICENSE-2.0

//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//

#ifndef SPL_DEBUG_DEBUG_H
#define SPL_DEBUG_DEBUG_H

#if defined(SPL_PRAGMA_ONCE_SUPPORTED)
    #pragma once // Some compilers (e.g. VC++) benefit significantly from using this. We've measured 3-4% build speed
                 // improvements in apps as a result. (EASTL)
#endif

#include <SPLB/internal/config.h>

#ifdef SPL_DEBUG
/**
 * @brief Printf with a condition for debug.
 * 
 * @param fmt format string.
 * @param ... args.
 */
void dbprintf(const char* fmt, ...);
#else
static inline void dbprintf(const char* fmt, ...)
{
    SPL_UNUSED(fmt);
}
#endif

#endif
