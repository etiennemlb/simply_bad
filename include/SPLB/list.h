//
//    @file linked_list.h
//    @author Etienne Malaboeuf
//    @brief
//    @version 0.1
//    @date 2020-03-04
//
//    Copyright 2020 Etienne Malaboeuf

//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at

//        http://www.apache.org/licenses/LICENSE-2.0

//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//

#ifndef SPL_LIST_H
#define SPL_LIST_H

#if defined(SPL_PRAGMA_ONCE_SUPPORTED)
    #pragma once // Some compilers (e.g. VC++) benefit significantly from using this. We've measured 3-4% build speed
                 // improvements in apps as a result. (EASTL)
#endif

#include <SPLB/internal/config.h>

#if defined(SPL_LIST_USE_POOL)
    #include <SPLB/pool.h>
#endif

////////////////////////////////////////////////////////////////////////////////
// Types
////////////////////////////////////////////////////////////////////////////////

typedef splb_SizeType_t       splb_list_SizeType_t;
typedef splb_ValueType_t      splb_list_ValueType_t;
typedef splb_DifferenceType_t splb_list_DifferenceType_t;

////////////////////////////////////////////////////////////////////////////////
// Node
////////////////////////////////////////////////////////////////////////////////

typedef struct splb_list_Node_t splb_list_Node_t;

struct splb_list_Node_t
{
    splb_list_Node_t*     prev_;
    splb_list_Node_t*     next_;
    splb_list_ValueType_t value_;
};

#if defined(SPL_LIST_USE_POOL)
    #define SPL_LIST_POOL_BLOCK_SIZE (sizeof(splb_list_Node_t))
#endif

////////////////////////////////////////////////////////////////////////////////
// Iterator
////////////////////////////////////////////////////////////////////////////////

typedef struct
{
    splb_list_Node_t* current_node_;
} splb_list_Iterator_t;

splb_list_ValueType_t* splb_list_IteratorGet(const splb_list_Iterator_t* it);

splb_list_Iterator_t splb_list_IteratorNext(splb_list_Iterator_t* it);
splb_list_Iterator_t splb_list_IteratorPrev(splb_list_Iterator_t* it);

splb_list_Iterator_t* splb_list_IteratorIncrement(splb_list_Iterator_t* it);
splb_list_Iterator_t* splb_list_IteratorDecrement(splb_list_Iterator_t* it);

int splb_list_IteratorEquals(const splb_list_Iterator_t* a, const splb_list_Iterator_t* b);
int splb_list_IteratorNotEquals(const splb_list_Iterator_t* a, const splb_list_Iterator_t* b);

splb_list_DifferenceType_t splb_list_IteratorDistance(const splb_list_Iterator_t* a, const splb_list_Iterator_t* b);

////////////////////////////////////////////////////////////////////////////////
// List base
////////////////////////////////////////////////////////////////////////////////

typedef struct
{
    splb_list_Node_t list_carrier_node_; // It's next points to the first node. It's prev, to the last.
#if defined(SPL_LIST_CACHE_SIZE)
    splb_list_SizeType_t size_;
#endif

#if defined(SPL_LIST_USE_POOL)
    splb_ObjectPool_t* node_pool_;
#endif
} splb_list_Base_t;

////////////////////////////////////////////////////////////////////////////////
// List
////////////////////////////////////////////////////////////////////////////////

typedef struct
{
    splb_list_Base_t base_;
} splb_List_t;

#if defined(SPL_LIST_USE_POOL)
int splb_list_Init(splb_List_t* list, splb_ObjectPool_t* pool);
int splb_list_InitNVal(splb_List_t*                 list,
                       splb_ObjectPool_t*           pool,
                       splb_list_SizeType_t         n,
                       const splb_list_ValueType_t* val);
#else
int splb_list_Init(splb_List_t* list);
int splb_list_InitNVal(splb_List_t* list, splb_list_SizeType_t n, const splb_list_ValueType_t* val);
#endif
void splb_list_DeInit(splb_List_t* list);

splb_list_Iterator_t splb_list_Begin(const splb_List_t* list);
splb_list_Iterator_t splb_list_End(/* const */ splb_List_t* list);

int                  splb_list_Empty(const splb_List_t* list);
splb_list_SizeType_t splb_list_Size(/* const */ splb_List_t* list);

splb_list_ValueType_t* splb_list_Front(const splb_List_t* list);
splb_list_ValueType_t* splb_list_Back(const splb_List_t* list);

void splb_list_PushFront(splb_List_t* list, const splb_list_ValueType_t* value);
void splb_list_PushBack(splb_List_t* list, const splb_list_ValueType_t* value);

void splb_list_PopFront(splb_List_t* list);
void splb_list_PopBack(splb_List_t* list);

splb_list_Iterator_t splb_list_Insert(splb_List_t* list, splb_list_Iterator_t pos, const splb_list_ValueType_t* value);

splb_list_Iterator_t splb_list_EraseIt(splb_List_t* list, splb_list_Iterator_t pos);
splb_list_Iterator_t splb_list_EraseRange(splb_List_t* list, splb_list_Iterator_t first, splb_list_Iterator_t last);

void splb_list_Clear(splb_List_t* list);

void splb_list_Reverse(splb_List_t* list);

// merge


#endif
