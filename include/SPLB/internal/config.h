//
//    @file config.h
//    @author Etienne Malaboeuf
//    @brief
//    @version 0.1
//    @date 2020-03-04
//
//    Copyright 2020 Etienne Malaboeuf

//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at

//        http://www.apache.org/licenses/LICENSE-2.0

//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//

#ifndef SPL_INTERNAL_CONFIG_H
#define SPL_INTERNAL_CONFIG_H

#if defined(SPL_PRAGMA_ONCE_SUPPORTED)
    #pragma once // Some compilers (e.g. VC++) benefit significantly from using this. We've measured 3-4% build speed
                 // improvements in apps as a result. (EASTL)
#endif

#include <stddef.h>
#include <stdint.h>

////////////////////////////////////////////////////////////////////////////////
// Platform defines
////////////////////////////////////////////////////////////////////////////////

#if defined(__unix__)
    #define SPL_OS_IS_UNIX
#elif defined(_WIN32) || defined(WIN32) || defined(_WIN64) || defined(WIN64)
    #define SPL_OS_IS_WINDOWS
#endif

////////////////////////////////////////////////////////////////////////////////
// Default defines
////////////////////////////////////////////////////////////////////////////////

// List.
#define SPL_LIST_USE_POOL
// #define SPL_LIST_CACHE_SIZE

// Deque.

// Pool.

// Bench.


////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// Tricks defines
////////////////////////////////////////////////////////////////////////////////

#define SPL_UNUSED(arg) ((void)arg);

////////////////////////////////////////////////////////////////////////////////

typedef size_t    splb_SizeType_t;
typedef long long splb_SignedSizeType_t;
typedef long long splb_IdxType_t;
typedef int32_t   splb_ValueType_t;
typedef ptrdiff_t splb_DifferenceType_t;
typedef uint64_t  splb_TimeNs_t;
typedef uint64_t  splb_RandU64_t;


#endif
