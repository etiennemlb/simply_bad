//
//    @file deque.h
//    @author Etienne Malaboeuf
//    @brief
//    @version 0.1
//    @date 2020-03-04
//
//    Copyright 2020 Etienne Malaboeuf

//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at

//        http://www.apache.org/licenses/LICENSE-2.0

//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//

#ifndef SPL_DEQUE_H
#define SPL_DEQUE_H

#if defined(SPL_PRAGMA_ONCE_SUPPORTED)
    #pragma once // Some compilers (e.g. VC++) benefit significantly from using this. We've measured 3-4% build speed
                 // improvements in apps as a result. (EASTL)
#endif

#include <SPLB/internal/config.h>

////////////////////////////////////////////////////////////////////////////////
// Types
////////////////////////////////////////////////////////////////////////////////

typedef splb_SizeType_t       splb_deque_SizeType_t;
typedef splb_ValueType_t      splb_deque_ValueType_t;
typedef splb_DifferenceType_t splb_deque_DifferenceType_t;

// Needs to be a power of two in order to increase addr calculation.
#define SPL_DEQUE_DEFAULT_SUBARRAY_SIZE                                                                                \
    ((sizeof(splb_deque_ValueType_t) <= 4) ?                                                                           \
         64 :                                                                                                          \
         ((sizeof(splb_deque_ValueType_t) <= 8) ?                                                                      \
              32 :                                                                                                     \
              ((sizeof(splb_deque_ValueType_t) <= 16) ? 16 : ((sizeof(splb_deque_ValueType_t) <= 32) ? 8 : 4))))

////////////////////////////////////////////////////////////////////////////////
// Iterator
////////////////////////////////////////////////////////////////////////////////

typedef struct
{
    splb_deque_ValueType_t*  current_;
    splb_deque_ValueType_t*  begin_;
    splb_deque_ValueType_t*  end_;
    splb_deque_ValueType_t** current_sub_array_ptr_;
} splb_deque_Iterator_t;

splb_deque_ValueType_t* splb_deque_IteratorGet(const splb_deque_Iterator_t* it);

splb_deque_Iterator_t* splb_deque_IteratorIncrement(splb_deque_Iterator_t* it);
splb_deque_Iterator_t* splb_deque_IteratorDecrement(splb_deque_Iterator_t* it);

splb_deque_Iterator_t* splb_deque_IteratorAdd(splb_deque_Iterator_t* it, splb_deque_DifferenceType_t n);
splb_deque_Iterator_t* splb_deque_IteratorMinus(splb_deque_Iterator_t* it, splb_deque_DifferenceType_t n);

int splb_deque_IteratorEquals(const splb_deque_Iterator_t* a, const splb_deque_Iterator_t* b);
int splb_deque_IteratorNotEquals(const splb_deque_Iterator_t* a, const splb_deque_Iterator_t* b);

int splb_deque_IteratorSupp(const splb_deque_Iterator_t* a, const splb_deque_Iterator_t* b);
int splb_deque_IteratorLess(const splb_deque_Iterator_t* a, const splb_deque_Iterator_t* b);

int splb_deque_IteratorLessOrEquals(const splb_deque_Iterator_t* a, const splb_deque_Iterator_t* b);
int splb_deque_IteratorSuppOrEquals(const splb_deque_Iterator_t* a, const splb_deque_Iterator_t* b);

splb_deque_DifferenceType_t splb_deque_IteratorDistance(const splb_deque_Iterator_t* a, const splb_deque_Iterator_t* b);

////////////////////////////////////////////////////////////////////////////////
// Deque base
////////////////////////////////////////////////////////////////////////////////

typedef struct
{
    splb_deque_ValueType_t** ptr_array_;
    splb_deque_SizeType_t    ptr_array_size_;
    splb_deque_Iterator_t    it_begin_;
    splb_deque_Iterator_t    it_end_;
} splb_deque_Base_t;

////////////////////////////////////////////////////////////////////////////////
// Deque
////////////////////////////////////////////////////////////////////////////////

typedef struct
{
    splb_deque_Base_t base;
} splb_Deque_t;

int  splb_deque_Init(splb_Deque_t* deque);
int  splb_deque_InitN(splb_Deque_t* deque, splb_deque_SizeType_t n);
void splb_deque_DeInit(splb_Deque_t* deque);

// Assign n times the value *value
// int splb_deque_AssignN(splb_Deque_t* deque,
//                          splb_deque_SizeType_t n,
//                          const splb_deque_ValueType_t* value);

// Assign one time the values in the array of n object
// int splb_deque_AssignFromArray(splb_Deque_t* deque,
//                                  splb_deque_SizeType_t array_size,
//                                  const splb_deque_ValueType_t* array);

splb_deque_Iterator_t splb_deque_Begin(const splb_Deque_t* deque);
splb_deque_Iterator_t splb_deque_End(const splb_Deque_t* deque);

// splb_deque_Iterator_t splb_deque_RBegin(splb_Deque_t* deque);
// splb_deque_Iterator_t splb_deque_REnd(splb_Deque_t* deque);

int                   splb_deque_Empty(const splb_Deque_t* deque);
splb_deque_SizeType_t splb_deque_Size(const splb_Deque_t* deque);

// int splb_deque_ResizeWithVal(splb_Deque_t* deque,
//                                splb_deque_SizeType_t n,
//                                const splb_deque_ValueType_t* value);
// int splb_deque_Resize(splb_Deque_t* deque,
//                         splb_deque_SizeType_t n);

// int splb_deque_ShrinkToFit(splb_Deque_t* deque);
// int splb_deque_SetCapacity(splb_Deque_t* deque,
//                              splb_deque_SizeType_t n);

splb_deque_ValueType_t* splb_deque_Get(const splb_Deque_t* deque, splb_deque_SizeType_t n);
splb_deque_Iterator_t   splb_deque_At(const splb_Deque_t* deque, splb_deque_SizeType_t n);

splb_deque_ValueType_t* splb_deque_Front(const splb_Deque_t* deque);
splb_deque_ValueType_t* splb_deque_Back(const splb_Deque_t* deque);

int splb_deque_PushFront(splb_Deque_t* deque, const splb_deque_ValueType_t* value);
int splb_deque_PushBack(splb_Deque_t* deque, const splb_deque_ValueType_t* value);

int splb_deque_PopFront(splb_Deque_t* deque);
int splb_deque_PopBack(splb_Deque_t* deque);

splb_deque_Iterator_t splb_deque_Insert(splb_Deque_t*                 deque,
                                        splb_deque_Iterator_t         pos,
                                        const splb_deque_ValueType_t* value);

// int splb_deque_InsertN(splb_Deque_t* deque,
//                          splb_deque_Iterator_t pos,
//                          splb_deque_SizeType_t n,
//                          const splb_deque_ValueType_t* value);
// int splb_deque_InsertFromArray(splb_Deque_t* deque,
//                                  splb_deque_Iterator_t pos,
//                                  splb_deque_SizeType_t array_size,
//                                  const splb_deque_ValueType_t* array);

splb_deque_Iterator_t splb_deque_EraseIt(splb_Deque_t* deque, splb_deque_Iterator_t pos);

// int splb_deque_EraseRange(splb_Deque_t* deque,
//                             splb_deque_Iterator_t first,
//                             splb_deque_Iterator_t last);

void splb_deque_Clear(splb_Deque_t* deque);

#endif
