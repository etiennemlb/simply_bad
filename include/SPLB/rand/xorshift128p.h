/**
 * @file xorshift128p.h
 * @author Wikipedia
 * @brief Crappy xorshift128+ implementation.
 * @version 0.1
 * @date 2020-02-15
 *
 */

#ifndef SPL_RAND_XORSHIFT128P_H
#define SPL_RAND_XORSHIFT128P_H

#if defined(SPL_PRAGMA_ONCE_SUPPORTED)
    #pragma once // Some compilers (e.g. VC++) benefit significantly from using this. We've measured 3-4% build speed
                 // improvements in apps as a result. (EASTL)
#endif

#include <SPLB/internal/config.h>

typedef struct
{
    splb_RandU64_t a;
    splb_RandU64_t b;
} splb_rand_xorshift128p_State_t;

/**
 * @brief Initialize a xorshift128+ state.
 *
 * @param state valid state ptr
 * @return int -1 if error, else 0. errno set appropriatly.
 */
int splb_rand_xorshift128p_InitState(splb_rand_xorshift128p_State_t* state, splb_RandU64_t seed);

/**
 * @brief Bad implementation of xorshift128+.
 *
 * @param state non zero state.
 * @return uint64_t pr generated number.
 */
splb_RandU64_t splb_rand_xorshift128p_NextU64(splb_rand_xorshift128p_State_t* state);

#endif
